# translation of block-module.po to Icelandic
# Icelandic translation of Drupal (modules/block.module)
# Generated from file: block.module,v 1.162.2.3 2005/05/22 12:50:09 dries
# Copyright 2005, Pjetur G. Hjaltason <pjetur@pjetur.net>.
# Pjetur G. Hjaltason <pjetur@pjetur.net>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: block-module\n"
"POT-Creation-Date: 2005-06-30 11:06+0200\n"
"PO-Revision-Date: 2005-11-10 00:44+0000\n"
"Last-Translator: Pjetur G. Hjaltason <pjetur@pjetur.net>\n"
"Language-Team: Icelandic <isl@molar.is>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural= n !=1;\n"
"X-Generator: KBabel 1.9.1\n"

#: modules/block.module:15
msgid "\n<p>Blocks are the boxes visible in the sidebar(s) of your web site. These are usually generated automatically by modules (e.g. recent forum topics), but you can also create your own blocks.</p>\n<p>The sidebar each block appears in depends on both which theme you are using (some are left-only, some right, some both), and on the settings in block management.</p>\n<p>The block management screen lets you specify the vertical sort-order of the blocks within a sidebar. You do this by assigning a weight to each block. Lighter blocks (smaller weight) \"float up\" towards the top of the sidebar. Heavier ones \"sink down\" towards the bottom of it.</p>\n<p>A block's visibility depends on:</p>\n<ul>\n<li>Its enabled checkbox. Disabled blocks are never shown.</li>\n<li>Its throttle checkbox. Throttled blocks are hidden during high server loads.</li>\n<li>Its path options. Blocks can be configured to only show/hide on certain pages.</li>\n<li>User settings. You can choose to let your users decide whether to show/hide certain blocks.</li>\n<li>Its function. Dynamic blocks (such as those defined by modules) may be empty on certain pages and will not be shown.</li>\n</ul>\n\n<h3>Administrator defined blocks</h3>\n<p>An administrator defined block contains content supplied by you (as opposed to being generated automatically by a module). Each admin-defined block consists of a title, a description, and a body which can be as long as you wish. The Drupal engine will render the content of the block.</p>"
msgstr "\n<p>Blokkir eru sýnilegar á hliðarslá(m) vefsins þíns. Þær eru venjulega búnar til sjálfkrafa af einingu (t.d. \"hverjir eru tengdir\"), en þú getur einnig búið til þínar eigin blokkir.</p>\n<p>Hvar blokkin er á hliðarsláinni veltur bæði á hvaða þema þú ert að nota (sum eru aðeins-vinstri, sum eru aðeins-hægri, sum hvoru tveggja), og á stillingum í blokkar-einingunni.</p>\n<p>Blokkar-stjórneiningin hjálpar þér við að raða blokkunum lóðrétt. Þú gerir þetta með því að gefa hverri blokk mismunandi þyngd. Léttari blokkir (minni þyngd) \"fljóta upp\" að toppi hliðarslárinnar. Þær þyngri \"sökkva niður\" að botni slárinnar.</p>\n<p>Sýnileiki blokkar veltur á:</p>\n<ul>\n<li>Hvort það sé merkt sýnilegt. Óvirkar blokkir eru aldrei sýndar.</li>\n<li>Álags-gátreiturinn. Álagsstýrðar blokkir eru faldar ef álag á miðlara er mikið.</li>\n<li>Slóðarvalkostur. Stýra má á hvaða síðum blokkir sjást.</li>\n<li>Stillingar notanda. Þú getur leyft notendum að ákveða hvort þeir vilji fela/sjá ákveðnar blokkir.</li>\n<li>Eiginleiki blokkarinnar. Breytilegar blokkir (eins og þær sem eru búnar til af einingum) geta verið tómar á sumum síðum og þi ekki sýndar.</li>\n</ul>\n\n<h3>Blokkir búnar til af kerfisstjóra</h3>\n<p>Blokk sem er búin til af vefstjóra inniheldur efni sem þú býrð til (ekki búið til sjálfkrafa af einingu). Allar blokkir erm eru búar til af vefstjóra innihalda titil, lýsingu, og innihald sem getur verið eins mikið og þú vilt. Drupal vefumsjónarkerfið mun teikna blokkina.</p>"

#: modules/block.module:31
msgid "Controls the boxes that are displayed around the main content."
msgstr "Stýrir blokkum sem eru birtar umhverfis megininnihald síðu."

#: modules/block.module:33
msgid "\n<p>Blocks are the boxes in the left and right side bars of the web site. They are made available by modules or created manually.</p>\n<p>Only enabled blocks are shown. You can position the blocks by deciding which side of the page they will show up on (sidebar) and in which order they appear (weight).</p>\n<p>If you want certain blocks to disable themselves temporarily during high server loads, check the 'Throttle' box. You can configure the auto-throttle on the <a href=\"%throttle\">throttle configuration page</a> after having enabled the throttle module.</p>\n"
msgstr "\n<p>Blokkir eru reitirnir í hægri og vinstri slánum á vefsíðunni. Þær eru búnar til af einingum eða smíðaðar handvirkt.</p>\n<p>Aðeins virkar blokkir eru sýndar. Þú getur staðsett blokkir með því að ákvarða á hvorri hlið síðunnar þær birtast (hliðarslá) og í hvaða röð þær birtast (þyngd).</p>\n<p>Ef þú vilt að ákveðnar blokkir hverfi við mikið álag á vefmiðlara, krossaðu þá við 'Álagsstýring' reitinn. Þú getur stillt álagsstýringuna á <a href=\"%throttle\">Álagsstýringarsíðunni</a> eftir að hafa virkjað álagsstýringar-eininguna.</p>\n"

#: modules/block.module:39
msgid "<p>Here you can create a new block. Once you have created this block you must make it active and give it a place on the page using <a href=\"%overview\">blocks</a>. The title is used when displaying the block. The description is used in the \"block\" column on the <a href=\"%overview\">blocks</a> page.</p>"
msgstr "<p>Hér getur þú búið til nýja blokk. Þegar þú hefur búið til þessa blokk þá verður þú að gera hana virka og setja á síðuna með því að nota <a href=\"%overview\">blokkir</a>. Titillinn er notaður þegar blokkin er birt. Lýsingin er notuð í \"blokkar\" dálknum á <a href=\"%overview\">yfirlitssíðunni</a>.</p>"

#: modules/block.module:62
msgid "configure block"
msgstr "sýsla með blokk"

#: modules/block.module:66
msgid "delete block"
msgstr "eyða blokk"

#: modules/block.module:70
msgid "add block"
msgstr "bæta við blokk"

#: modules/block.module:120
msgid "The block settings have been updated."
msgstr "Stillingar blokkar hafa verið uppfærðar."

#: modules/block.module:186
msgid "Block"
msgstr "Blokk"

#: modules/block.module:186
msgid "Sidebar"
msgstr "Hliðarslá"

#: modules/block.module:207
msgid "left"
msgstr "vinstri"

#: modules/block.module:207
msgid "right"
msgstr "hægri"

#: modules/block.module:229
msgid "Left sidebar"
msgstr "Vinstri hliðarslá"

#: modules/block.module:233
msgid "Right sidebar"
msgstr "Hægri hliðarslá"

#: modules/block.module:241;382
msgid "Save blocks"
msgstr "Vista blokkir"

#: modules/block.module:258;300;314;321
msgid "Save block"
msgstr "Vista blokk"

#: modules/block.module:267
msgid "The block configuration has been saved."
msgstr "Stillingar blokkarinnar hafa verið vistaðar."

#: modules/block.module:279
msgid "Block-specific settings"
msgstr "Blokkarstillingar"

#: modules/block.module:287
msgid "'%name' block"
msgstr "'%name - blokk"

#: modules/block.module:290
msgid "Custom visibility settings"
msgstr "Sérsniðnar reglur um sýnileika"

#: modules/block.module:290
msgid "Users cannot control whether or not they see this block."
msgstr "Notendur geta ekki stýrt hvort þessi blokk sé sýnileg."

#: modules/block.module:290
msgid "Show this block by default, but let individual users hide it."
msgstr "Sýna þessa blokk sjálfgefið, en leyfa notendum að fela hana."

#: modules/block.module:290
msgid "Hide this block by default but let individual users show it."
msgstr "Fela þessa blokk sjálfgefið, en leyfa notendum að gera hana sýnilega."

#: modules/block.module:290
msgid "Allow individual users to customize the visibility of this block in their account settings."
msgstr "Leyfa notendum að velja sýnileika þessarar blokkar í sínum notendastillingum."

#: modules/block.module:291
msgid "Show block on specific pages"
msgstr "Sýna blokk á völdum síðum"

#: modules/block.module:291
msgid "Show on every page except the listed pages."
msgstr "Sýna á öllum síðum, nema þeim sem eru á listanum."

#: modules/block.module:291
msgid "Show on only the listed pages."
msgstr "Sýna aðeins á völdum síðum."

#: modules/block.module:292
msgid "Pages"
msgstr "Síður"

#: modules/block.module:292
msgid "Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are '<em>blog</em>' for the blog page and '<em>blog/*</em>' for every personal blog. '<em>&lt;front&gt;</em>' is the front page."
msgstr "Sláðu inn eina Drupal slóð í hverja línu. Bókstafurinn '*' er algildisstafur. Dæmi slóðir eru '<em>blog</em>' fyrir Vefdagbókar síðuna og '<em>blog/*</em> fyrir allar vefdagbókar síðurnar. '<em>&lt;front&gt;</em>' er forsíðan."

#: modules/block.module:293
msgid "Restrict block to specific content types"
msgstr "Takmarka blokk við ákveðið innihald"

#: modules/block.module:293
msgid "Selecting one or more content types will cause this block to only be shown on pages of the selected types. This feature works alone or in conjunction with page specific visibility settings. For example, you can specify that a block only appear on book pages in the 'FAQ' path."
msgstr "Ef þú velur eina eða fleiri innihaldstegundir mun blokkin aðeins verða sýnd á síðum af þeirri tegund. Þetta gildir eitt og sér eða saman með sérvöldum síðum. Til dæmis getur þú valið að blokk birtist aðeins á bókar-síðum í 'SOS' slóðinni."

#: modules/block.module:295
msgid "User specific visibility settings"
msgstr "Notendaháður sýnileiki innihalds"

#: modules/block.module:296
msgid "Page specific visibility settings"
msgstr "Síðuháður sýnileiki innihalds"

#: modules/block.module:297
msgid "Content specific visibility settings"
msgstr "Sýnileiki innihalds"

#: modules/block.module:316
msgid "The new block has been added."
msgstr "Nýrri blokk hefur verið bætt við."

#: modules/block.module:338
msgid "The block %name has been deleted."
msgstr "Blokkinni %name hefur verið eytt."

#: modules/block.module:344
msgid "Are you sure you want to delete the block %name?"
msgstr "Ert þú viss um að þú viljir eyða blokk %name?"

#: modules/block.module:354
msgid "Block title"
msgstr "Blokkartitill"

#: modules/block.module:354
msgid "The title of the block as shown to the user."
msgstr "Titill blokkar eins og hún birtist notanda."

#: modules/block.module:356
msgid "Block body"
msgstr "Meginhluti blokkar"

#: modules/block.module:356
msgid "The content of the block as shown to the user."
msgstr "Innihald blokkar eins og hún birtist notanda."

#: modules/block.module:357
msgid "Block description"
msgstr "Blokkarlýsing"

#: modules/block.module:357
msgid "A brief description of your block. Used on the <a href=\"%overview\">block overview page</a>."
msgstr "Stutt lýsing á blokkinni þinni.Notað á <a href=\"%overview\">yfirlitsíðunni</a>."

#: modules/block.module:410
msgid "Block configuration"
msgstr "Blokkarstilling"

#: modules/block.module:47
msgid "administer blocks"
msgstr "sýsla með blokkir"

#: modules/block.module:0
msgid "block"
msgstr "blokk"

